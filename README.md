![Vizury Logo](https://bitbucket.org/vizury/vizury-huawei-sdk/raw/9e05398d6de4249d87b52bdbf0e3c472f0d0d671/resources/VizuryLogo.jpg)

## Summary
 This is Huawei SDK integration guide.
 
## Components
  * [Example app](#markdown-header-example-app)
  * [Basic Integration](#markdown-header-basic-integration)
	* [Getting the SDK](#markdown-header-getting-the-sdk)
	* [Manifest File Changes](#markdown-header-manifest-file-changes)
	* [Vizury SDK Initialization](#markdown-header-vizury-sdk-initialization)
	* [Event Logging](#markdown-header-event-logging)
	* [Click Tracking](#markdown-header-click-tracking)
  * [Push Notifications](#markdown-header-push-notifications)
  	* [Enabling Push services in HMS](#markdown-header-enabling-push-services-in-hms-appgallery-connect-agc)
  		* [Creating an app in AppGallery Connect](#markdown-header-creating-an-app-in-appgallery-connect-agc)
		* [Generating a Signing Certificate](#markdown-header-generating-a-signing-certificate)
		* [Generating a Signing Certificate Fingerprint](#markdown-header-generating-a-signing-certificate-fingerprint)
		* [Add Maven libraries and plugin](#markdown-header-add-maven-libraries-and-plugin)
		* [Configure project signature](#markdown-header-configure-project-signature)
		* [Add fingerprint certificate to AppGallery Connect](#markdown-header-add-fingerprint-certificate-to-appgallery-connect)
		* [Setting HUAWEI Push Kit Parameters ](#markdown-header-setting-huawei-push-kit-parameters)
	* [Add HMS SDK](#markdown-header-add-hms-sdk)
		* [Download the agconnect-services.json file](#markdown-header-download-the-agconnect-servicesjson-file)
		* [Gradle changes](#markdown-header-gradle-changes)
		* [Configuring Obfuscation Scripts](#markdown-header-configuring-obfuscation-scripts)
		* [Manifest changes](#markdown-header-manifest-changes)
  * [Additional configurations](#markdown-header-additional-configurations)
  	* [Offline caching](#markdown-header-offline-caching)

## Example app

Examples on how the Vizury SDK can be integrated.
`examples/HelloVizury` is a sample app having a basic integration with vizury SDK.


## Basic Integration

### Getting the SDK
Vizury Huawei Library is available as a maven dependency. Add jcenter in repositories, in your top level build.gradle file
```
repositories {
    jcenter()
    mavenCentral()
}
```
Add the following dependency in your build.gradle file under app module

```
implementation 'com.vizury.mobile:VizurySDK:6.3.1-huawei'
//for beta version test 
implementation 'com.vizury.mobile:VizurySDK:6.3.2-huawei-beta-1.0'

//needed for VizurySDK
implementation 'joda-time:joda-time:2.9.4'
implementation 'com.google.code.gson:gson:2.8.5'
```


### Manifest File Changes

In the Package Explorer open the AndroidManifest.xml of your Android project and make the following changes


Add the below meta tags, replacing the place holders with associated values.

```xml
<meta-data
	android:name="Vizury.VRM_ID"
	android:value="{PACKAGE_ID}" />
<meta-data
	android:name="Vizury.SERVER_URL"
	android:value="{SERVER_URL}" />
<meta-data
	android:name="Vizury.DEBUG_MODE"
	android:value="{true/false}" /> 
```

`PACKAGE_ID` and `SERVER_URL` will be provided by the vizury account manager.
`DEBUG_MODE` is used to show vizury debug/error/info logs. Set this to false when going to production.

### Vizury SDK Initialization

In your MainActivity.java inside onCreate(Bundle SavedInstance) add following code snippet

```java
import com.vizury.mobile.VizuryHelper;
@Override
protected void onCreate(Bundle savedInstanceState) {
	//your code		    
	VizuryHelper.getInstance(getApplicationContext()).init();
	//your code
}
```

### Event Logging

When a user browse through the app, various activities happen e.g. visiting a product, adding the product to cart, making purchase, etc. These are called events. Corresponding to each event, app needs to pass certain variables to the SDK which the SDK will automatically pass to Vizury servers.

Add the following code in each view where the event is to be tracked - 

```java
 VizuryHelper.getInstance(context).logEvent(eventname, attributes);
```
```
Where 
 eventname   : name of the event
 attributes  : AttributeBuilder object containing the attributes. You can set multiple attributes.
```

Eg.
```java
import com.vizury.mobile.VizuryHelper;

AttributeBuilder builder = new AttributeBuilder.Builder()
	.addAttribute("pid", "AFGEMSBBLL")
	.addAttribute("quantity", "1")
	.addAttribute("price", "876")
	.addAttribute("category","clothing")
	.build();

VizuryHelper.getInstance(context).logEvent("product page", builder);
```

You can also pass a JSONObject or a JSONArray as an attribute. 
For passing a JSONObject or JSONArray you can do something like this :

```java
JSONObject attrs;           // your attributes are in a JSONObject.
AttributeBuilder builder = new AttributeBuilder.Builder()
	.addAttribute("productAttributes", attrs.toString())
	.build(); 
```

### Click tracking

When a user receives a notification and the notification has been clicked, the corresponding click event has to be tracked,tracking data will be passed from the notification to the app , and the app needs to pass the intent to the SDK which the SDK will automatically pass to Vizury servers.

Add the following code in each activity 


```java
 Utils.getInstance(getApplicationContext()).handleClick(getIntent());
```
Add the following changes in each activity tag inside AndroidManifest.xml 


```xml
			<intent-filter>
                <action android:name="android.intent.action.VIEW" />
                <category android:name="android.intent.category.DEFAULT" />
                <category android:name="android.intent.category.BROWSABLE" />
                <data
                    android:host="your host name"
                    android:path="/path of the intent or activity"
                    android:scheme="your scheme" />
            </intent-filter>

```
Eg.


```xml
		<activity
            android:name=".SecondActivity"
            android:label="@string/title_activity_second"
            android:theme="@style/AppTheme.NoActionBar">
            <intent-filter>
                <action android:name="android.intent.action.VIEW" />
                <category android:name="android.intent.category.DEFAULT" />
                <category android:name="android.intent.category.BROWSABLE" />
                <data
                    android:host="vizury"
                    android:path="/home"
                    android:scheme="hellovizury" />
            </intent-filter>
        </activity>

```

# Push Notifications

Vizury sends push notifications to Huawei devices using HMS - AppGallery Connect (AGC).

## Enabling Push services in HMS - AppGallery Connect (AGC). 

### Creating an app in AppGallery Connect (AGC)

When creating the app, you will need to enter the app name, app category, default language, and signing certificate fingerprint.
After the app has been created, you will be able to obtain the basic configurations for the app, for example, the app ID and app secret, which will then be used in subsequent development activities.
   
 1)Sign in to HUAWEI Developer[huawei-developer] and click Console.   
 2)Click the HUAWEI AppGallery card and access AppGallery Connect.   
 3)On the AppGallery Connect page, click My apps.   
 4)On the displayed My apps page, click New.   
 5)Enter the App name, select App category (options: App and Game), and select Default language as needed.   
 6)Upon successful app creation, the App information page will automatically display.    
	There you can find the App ID and App secret that are assigned by the system to your app.   
  For further reference [link]
	
	
###  Generating a Signing Certificate
During this step, you will create a new signature file in Android Studio, which will be used for generating the SHA256 fingerprint for your app.

 1)In the menu bar of the Android Studio project that you have created, go to Build>Generate Signed Bundle/APK...   
 2)On the Generate Signed Bundle or APK page, select APK and click Next.   
 3)If you've already had a signature file, click Choose existing... ,   
	3.1)select the signature file, and specify Key store password,Key alias, and Key password for it. After completing this, click Next.   
	3.2)If you don't have a signature file, click Create new...   
	Specify relevant information including Key store path, Password, and Key Alias.   
	3.3)After successfully creating the signature file, you will find the signature file information on the automatically displayed Generate Signed Bundle or APK page. Click Next.   
 4)On the displayed page, select V1 and V2 next to Signature Versions, and then click Finish. You have now created a signed APK (This APK file can be used to upload the generated package name).   

### Generating a Signing Certificate Fingerprint

 1)On the terminal, enter keytool to view the signature file and run the command   
		Windows - keytool -list -v -keystore /path/theNameGivenDuringGeneratingASigningCertificate.jks   
		Mac OS - keytool -list -v -keystore /path/theNameGivenDuringGeneratingASigningCertificate.jks   
	2)Enter the password of the signature file keystore in the information area. The password is the password used to generate the signature file.   
	3)Obtain the SHA256 fingerprint from the result.   
	
![agconnect_download](https://bitbucket.org/vizury/vizury-huawei-sdk/raw/d11bf918ae118687a4489a12ceda7a32f6c4c027/resources/sha256.png)
	
	
	
### Add Maven libraries and plugin

1)In Android Studio root-level (project-level) build.gradle file, add rules to include the HUAWEI agcp plugin and HUAWEI Maven repository

		```
		buildscript {
    		repositories {
				// Check that you have the following line (if not, add it)
				maven { url 'https://developer.huawei.com/repo/' } // HUAWEI Maven repository
    		}	
    		dependencies {
				// Add the following line
				classpath 'com.huawei.agconnect:agcp:1.3.1.300'  // HUAWEI agcp plugin
    		}
		}

		allprojects {
			repositories {
				// Check that you have the following line (if not, add it)
				maven { url 'https://developer.huawei.com/repo/' } // HUAWEI Maven repository
				// ...
			}
		}
		```
		
2)In your module (app-level) build.gradle file (usually app/build.gradle), add a line to the bottom of the file.	

			```
			apply plugin: 'com.android.application'
			// Add the following line
			apply plugin: 'com.huawei.agconnect'  // HUAWEI agconnect Gradle plugin
			android {
				// ...
			}

			dependencies {
				// ...
			}
			```
			
### Configure project signature

 1)Copy the generated signature file theNameGivenDuringGeneratingASigningCertificate.jks into the app folder.   
 2)Open your module (app-level) build.gradle file (usually app/build.gradle) ,   
		Add signature configuration information to the android closure of the build.gradle file   
	
	```
		signingConfigs {
			release {
				storeFile file('theNameGivenDuringGeneratingASigningCertificate.jks')
				keyAlias 'yourkealias'
				keyPassword 'yourpassword'
				storePassword 'yourstorepassword'
				v1SigningEnabled true
				v2SigningEnabled true
			}
		}

		buildTypes {
			release {
				signingConfig signingConfigs.release
				minifyEnabled false
				proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
			}
			debug {
				signingConfig signingConfigs.release
				debuggable true
			}
		}
		```

### Add fingerprint certificate to AppGallery Connect
 During this step, you will configure the generated SHA256 fingerprint in AppGallery Connect.   

  1)On the page for creating an application, click the Develop tab. or,    
	find the created project application in My Projects and choose Project Settings -> Convention. The Convention page is displayed.   
	2)Go to the App information section and Click to add the SHA256 certificate fingerprint.   
	3)Enter the generated SHA256 value in the text box.   
	4)Click √ to save the fingerprint.  

![agconnect_download](https://bitbucket.org/vizury/vizury-huawei-sdk/raw/d11bf918ae118687a4489a12ceda7a32f6c4c027/resources/shaimage.PNG)

### Setting HUAWEI Push Kit Parameters
 Follow the below steps   
1)Sign in to AppGallery Connect[app-gallery-connect] and select My projects.      
2)Find your project from the project list and click the app for which you need to enable a service on the project card.   
3)Go to Growing > Push Kit and click Enable now. In the dialog box that is displayed, click OK.   

![agconnect_download](https://bitbucket.org/vizury/vizury-huawei-sdk/raw/d11bf918ae118687a4489a12ceda7a32f6c4c027/resources/enable_push_services.PNG)
	

## Add HMS SDK

### Download the agconnect-services.json file.

![agconnect_download](https://bitbucket.org/vizury/vizury-huawei-sdk/raw/ca1f98aa9c8e4e29f031d92a826dcb7eb96ddaa6/resources/agconnect_download.png)

Move the downloaded agconnect-services.json file to the app directory of your Android Studio project.

![add_agconnect_services](https://bitbucket.org/vizury/vizury-huawei-sdk/raw/3ae0faeebead069c30f5429731db550c721c73be/resources/add_agconnect_services_json.png)

### Gradle changes
Project-level build.gradle (`<project>/build.gradle`):

```
buildscript {
  dependencies {
	// Add this line
	classpath 'com.huawei.agconnect:agcp:1.2.1.301'
  }
}
```
App-level build.gradle (`<project>/<app-module>/build.gradle`):

```
dependencies {
  // Add this line
  implementation 'com.huawei.hms:push:5.3.0.304'
  implementation 'com.huawei.hms:ads-identifier:3.4.28.305'
}
...
// Add to the bottom of the file
apply plugin: 'com.huawei.agconnect'
```

### Configuring Obfuscation Scripts
Open the obfuscation configuration file proguard-rules.pro of your Android Studio project.
Add configurations to exclude HUAWEI Push Kit SDK from obfuscation
```
-ignorewarnings
-keepattributes *Annotation*
-keepattributes Exceptions
-keepattributes InnerClasses
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
-keep class com.hianalytics.android.**{*;}
-keep class com.huawei.updatesdk.**{*;}
-keep class com.huawei.hms.**{*;}
-keep class com.huawei.hms.ads.** { *; }
-keep interface com.huawei.hms.ads.** { *; }
```
	
### Manifest changes

In the Package Explorer open the AndroidManifest.xml of your Android project and and make the following changes

Add the following meta-tags
```xml
<meta-data
	android:name="Vizury.NOTIFICATION_ICON"
	android:resource="{NOTIFICATION_ICON}" />
<meta-data
	android:name="Vizury.NOTIFICATION_ICON_SMALL"
	android:resource="{NOTIFICATION_ICON_SMALL}" />
```

`NOTIFICATION_ICON` is the icon that comes at the left of a notification

`NOTIFICATION_ICON_SMALL` is the icon that comes at the notification bar. This should be white icon on a transparent background

Refer [Android Notifications][android_notifications] and [Style Icons][style_icons] for more details.

Add the following services for receiving the hms token and messages.

```xml
<service
	android:name="com.vizury.mobile.Push.VizHmsMessagingService"
	android:exported="false">
	<intent-filter>
		<action android:name="com.huawei.push.action.MESSAGING_EVENT" />
	</intent-filter>
</service>
```

`Mandatory intent service for doing the heavy lifting`
```xml
<service 
	android:name="com.vizury.mobile.Push.VizIntentService"
	android:exported="false">
</service>
```

## Additional configurations

### Offline caching

If your app supports offline features and you want to send the user behaviour data to vizury while he is offline, add the following tag in the manifest.

```xml
<meta-data
	android:name="Vizury.DATA_CACHING"
	android:value="true"/>
```
[link]: https://developer.huawei.com/consumer/en/doc/development/HMS-Guides/iap-configuring-appGallery-connect
[app-gallery-connect]: https://developer.huawei.com/consumer/en/service/josp/agc/index.html
[huawei-developer]: https://developer.huawei.com/consumer/en/
[google_ad_id]:                 https://support.google.com/googleplay/android-developer/answer/6048248?hl=en
[google_play_services]:         http://developer.android.com/google/play-services/setup.html
[google_developer_console]:	https://developers.google.com/mobile/add?platform=android
[android_notifications]:	https://material.google.com/patterns/notifications.html
[style_icons]:			https://material.google.com/style/icons.html
